###################################################################################
# Stage 1: BUILD & TEST                                                           #
###################################################################################
FROM gradle:6.7.0-jdk11 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon
###################################################################################
# Stage 2: RUNTIME                                                          #
###################################################################################
FROM openjdk:8-jre-alpine
EXPOSE 8082
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
COPY --from=build /home/gradle/src/build/libs/*.jar songservice.jar
ENTRYPOINT ["java", "-jar", "songservice.jar"]
