Authoren: Damir Katovic und Christoph Untner
FH-Salzburg, ITS


# Funktionen von Songservice
+ Alle Songs suchen. 
+ Bestimmten Song nach songID suchen.
+ Neuen Song erstellen.
+ Bestehenden Song, nach songID gesucht, updaten.
+ Bestehenden Song löschen.

# Angebotene Schnittstelle


Bsp. http requests
+ GET http://localhost:8080/api/v1/songs
Gibt alle Songs zurück

+ GET http://localhost:8080/api/v1/songs/12
Gibt Song mit der SongId 12 zurück

+ POST http://localhost:8080/api/v1/songs
Content-Type: application/json
{
  "songId" : 123,
  "title" : "MyFirstSong",
  "genre" : "experimentel",
  "album" : "MyFirst",
  "artist": "Chris",
  "duration" : 300
}
Erzeugt Song mit den übergebenen Werten

+ PUT http://localhost:8080/api/v1/songs/123
Content-Type: application/json

{
  "songId": 123,
  "title" : "MyFirstUpdate",
  "genre" : "experimentel",
  "album" : "MySecond",
  "artist": "Chrisi",
  "duration" : 333
}
updated Song mit der ID 123


+ DELETE http://localhost:8080/api/v1/songs/123
Accept: application/json
Löscht Song mit der Id 123




# Song; Daten aus denen ein Song besteht
Ein Song hat folgende Felder:
+ id -> wird von der Datenbank vergeben
+ songId
+ title
+ genre
+ album
+ artist
+ duration

# http-client
In dem song-request.http sind Beispielrequests aufgelistet.

# SampleDataGenerator
In diesem File befindet sich eine Methode die 10 Beispielsongs generiert. 

# Layer
Presentation Layer  = SongController
Business Layer      = SongService(interface) und in SongServiceImpl sind die Methoden implementiert
Persistence Layer   = Songrepository
Database            = Aktuell eine H2 Memory Datenbank

#Compiler
Complier Version 8 notwendig, da alpine spätere Versionen von jdk nicht unterstützt.

#Dockerfile
Dockerfile für 2 Images vorhanden. Brauchbar für Docker Compose.

# Multipart Upload
Upload mittels Multipart eingebaut. Empfangen werden die Files als byte array.
Diese werden als String konvertiert und BASE64 codiert. Als kodierter String 
wird es an MediaStorage Service weitergeleitet. Als Response wird die SongID zurückgeliefert.
Diese ID wird zu den Metadaten gespeichert.

# LoadBalanced
Mittels Service Discovery wird der songservice gefunden und ide API Url zum MediaStorage wird mittels
dynamischen URL gefunden.

#SonarCube Add Project
New Project created
