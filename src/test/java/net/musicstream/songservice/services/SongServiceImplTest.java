package net.musicstream.songservice.services;

import net.musicstream.songservice.domain.Song;
import net.musicstream.songservice.repositories.MediaServiceRestTemplate;
import net.musicstream.songservice.repositories.SongRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;


@ExtendWith(MockitoExtension.class)
class SongServiceImplTest {


    private SongServiceImpl service;

    @Mock
    private SongRepository songRepository;

    @Mock
    private MediaServiceRestTemplate mediaServiceRestTemplate;

    // SongRepository songRepository, MediaServiceRestTemplate mediaServiceRestTemplate)

    @BeforeEach
    void setUp() {
        service = new SongServiceImpl(songRepository, mediaServiceRestTemplate);
    }

    Song testSongs = new Song((long) 1,234,"Junit","rokk","huhi","MrJunit",50);
    Song testSongs2 = new Song((long) 2,235,"Junit2","rokk2","huhi2","MrJunit2",51);
    List<Song> requestedSongs = new ArrayList<>();


    @Test
    void findAll_ok()
    {
        requestedSongs.add(testSongs);
        requestedSongs.add(testSongs2);
        //mock
        Mockito.when(songRepository.findAll()).thenReturn(
                Arrays.asList(
                        Song.builder().album("huhi").duration(50).genre("rokkk").songId(234).title("Junit").artist("MrJunit").id((long)1).build(),
                        Song.builder().album("huhi2").duration(51).genre("rokkk2").songId(235).title("Junit2").artist("MrJunit2").id((long)2).build())
        );
        // Execute Test
        //List<Song> song = service.findAll();
        List<Song> song = songRepository.findAll();
        Song MokTestSong = song.get(0);
        if(MokTestSong.getAlbum()==testSongs.getAlbum()) {
            assertTrue(true);
        }
        else{
            assertTrue(false);
        }
        //assertTrue( song.equals(requestedSongs) ) ;
       // if(song.equals(requestedSongs))
        //    assertTrue(false);
       // else
       //     assertFalse(false);




        //TODO. findet keine Asserts
        // https://www.youtube.com/watch?list=PLOgKiFSvGhWSWAoyV0lKE5MC_9at61GM2&v=hQ87EMuCQWc&ab_channel=DavidSch%C3%B6ninger
        // min 30 noch mal schauen
    }
}
