package net.musicstream.songservice;

import lombok.extern.slf4j.Slf4j;
import net.musicstream.songservice.domain.Song;
import net.musicstream.songservice.repositories.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Slf4j
@Component
public class SampleDataGenerator implements CommandLineRunner {

    private SongRepository songRepository;

    @Autowired
    public SampleDataGenerator(SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Generating test data.");
        List<Song> songs = generateSampleSongs(10);
    }

    private List<Song> generateSampleSongs(int numberOfSongs) {
        Random random = new Random();
        List<Song> songs = new ArrayList<>();
        for (int i = 1; i <= numberOfSongs; i++) {
            songs.add(Song.builder()
                    .genre("Rock")
                    .title("Song" + i)
                    .album("Rock me")
                    .artist("Tom")
                    .duration(150 + random.nextInt(70))
                    .songId(10+i)
                    .build());
        }
        return songRepository.saveAll(songs);
    }
}
