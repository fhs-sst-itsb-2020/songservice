package net.musicstream.songservice.services;

import lombok.SneakyThrows;
import net.musicstream.songservice.domain.Song;
import net.musicstream.songservice.repositories.MediaServiceRestTemplate;
import net.musicstream.songservice.repositories.MediaStorageRequest;
import net.musicstream.songservice.repositories.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.Base64;
import java.util.List;

@Service
public class SongServiceImpl implements SongService {

    private SongRepository songRepository;
    private MediaServiceRestTemplate mediaServiceRestTemplate;


    @Autowired
    public SongServiceImpl(SongRepository songRepository, MediaServiceRestTemplate mediaServiceRestTemplate) {
        this.songRepository = songRepository;
        this.mediaServiceRestTemplate = mediaServiceRestTemplate;
    }

    @Override
    public List<Song> findAll() {
        return songRepository.findAll();
    }

    @Override
    public Song findSongBySongId(long songId) {
        return songRepository.findSongBySongId(songId);
    }

    @Override
    @SneakyThrows
    public Song createSong(Song song, @RequestParam("file") MultipartFile file) {
        //Send file to other service
        MediaStorageRequest base64 = new MediaStorageRequest(Base64.getEncoder().encodeToString(file.getBytes()));
        mediaServiceRestTemplate.createSong(base64);
        return songRepository.save(song);
    }

    @Override
    public Song updateSong(Song newSongValue, long songId) {
        Song song = findSongBySongId(songId);

        song.setSongId(newSongValue.getSongId());
        song.setAlbum(newSongValue.getAlbum());
        song.setArtist(newSongValue.getArtist());
        song.setDuration(newSongValue.getDuration());
        song.setGenre(newSongValue.getGenre());

        song.setTitle(newSongValue.getTitle());

        return songRepository.save(song);
    }

    @Override
    public void deleteSong(long songId) {
        Song song = findSongBySongId(songId);
        mediaServiceRestTemplate.deleteSong(songId);
        songRepository.delete(song);
    }
}
