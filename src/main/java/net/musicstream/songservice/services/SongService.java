package net.musicstream.songservice.services;

import lombok.SneakyThrows;
import net.musicstream.songservice.domain.Song;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface SongService {
    List<Song> findAll();
    Song findSongBySongId(long songId);

    @SneakyThrows
    Song createSong(Song song, @RequestParam("file") MultipartFile file);

    Song updateSong(Song newSongValue, long songId);
    void deleteSong(long songId);
}
