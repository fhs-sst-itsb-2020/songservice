package net.musicstream.songservice.services;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

public interface JwtService {

    Jws<Claims> parseAndValidate(String jwt);
}
