package net.musicstream.songservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SongserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SongserviceApplication.class, args);
    }


}
