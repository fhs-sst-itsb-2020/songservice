package net.musicstream.songservice.controllers;

import lombok.SneakyThrows;
import net.musicstream.songservice.domain.Song;
import net.musicstream.songservice.services.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/songservice/api/v1/songs")
public class SongController {

    private SongService songService;

    @Autowired
    public SongController(SongService songService) {
        this.songService = songService;
    }

    @GetMapping
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public List<Song> getAll() {
        return songService.findAll();
    }

    @GetMapping("/{songId}")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public Song getBySongId(@PathVariable long songId) {
        return songService.findSongBySongId(songId);
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    @Secured("ROLE_ADMIN")
    @SneakyThrows
    public Song createNew(Song song, @RequestParam("file") MultipartFile file) {

        songService.createSong(song, file);
        return null;
    }

    @PutMapping("/{songId}")
    @Secured("ROLE_ADMIN")
    public Song updateSong(@PathVariable long songId, @RequestBody Song newSongValue){
        return songService.updateSong(newSongValue, songId);
    }

    @DeleteMapping("/{songId}")
    @Secured("ROLE_ADMIN")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteSongForSongId(@PathVariable long songId){
        songService.deleteSong(songId);
    }


}
