package net.musicstream.songservice.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Entity
@AllArgsConstructor @NoArgsConstructor @Getter @Setter @Builder
public class Song {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // unique attributes for a song
    private long songId;
    private String title;

    private String genre;

    @Column(nullable = false)
    private String album;

    @Column(nullable = false)
    private String artist;

    @Min(0)
    private long duration;

}
