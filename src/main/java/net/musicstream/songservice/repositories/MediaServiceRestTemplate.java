package net.musicstream.songservice.repositories;


import net.musicstream.songservice.domain.Song;
import org.apache.catalina.filters.ExpiresFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityNotFoundException;

@Service
public class MediaServiceRestTemplate {

    private final RestTemplate restTemplate;

    @Autowired
    public MediaServiceRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public void deleteSong(long songId){
        restTemplate.delete("http://mediastorage/api/Songs/" + songId, Song.class);

    }

    public Long createSong(MediaStorageRequest file){

        MediaStorageResponse fileID = new MediaStorageResponse();
        ResponseEntity<Long> response = restTemplate.postForEntity("http://mediastorage/api/Songs/", file, long.class);
        fileID.setFileId(response.getBody());
        return fileID.getFileId();

    }

   public ExpiresFilter.XHttpServletResponse updateSong(MediaStorageRequest song, long songId){
        ExpiresFilter.XHttpServletResponse myHttp;
        int test = 0;
        restTemplate.put("http://mediastorage/api/Songs/"+ songId, song, Song.class, test);
        if ( test == ExpiresFilter.XHttpServletResponse.SC_BAD_REQUEST || test == ExpiresFilter.XHttpServletResponse.SC_NOT_FOUND)
            throw new EntityNotFoundException();
        else
            return null;
    }




}
