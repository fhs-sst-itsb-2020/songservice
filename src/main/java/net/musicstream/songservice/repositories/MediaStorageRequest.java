package net.musicstream.songservice.repositories;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor @NoArgsConstructor @Getter
public class MediaStorageRequest {

    private String fileBase64;

}
