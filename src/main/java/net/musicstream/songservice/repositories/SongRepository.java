package net.musicstream.songservice.repositories;

import net.musicstream.songservice.domain.Song;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface SongRepository extends JpaRepository<Song, Long> {

    @Query("select s from Song s where s.id = ?1 ")
    Song findSongBySongId(long songId);
}
