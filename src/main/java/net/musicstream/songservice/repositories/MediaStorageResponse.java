package net.musicstream.songservice.repositories;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class MediaStorageResponse {

    private Long fileId;
    private Boolean createOK;

}
